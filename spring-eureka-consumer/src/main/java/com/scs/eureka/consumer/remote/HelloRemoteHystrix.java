package com.scs.eureka.consumer.remote;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class HelloRemoteHystrix implements HelloRemote{

    @Override
    public String hello(@RequestParam(value = "name") String name) {
    	log.warn("找不到生产者，熔断器开启");
        return "hello" +name+", this messge send failed ";
    }
}