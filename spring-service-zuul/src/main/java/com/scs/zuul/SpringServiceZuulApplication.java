package com.scs.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

import com.scs.zuul.Filter.TokenFilter;

@EnableZuulProxy
@SpringBootApplication
public class SpringServiceZuulApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringServiceZuulApplication.class, args);
	}
	
	@Bean
	public TokenFilter tokenFilter() {
		return new TokenFilter();
	}
}
